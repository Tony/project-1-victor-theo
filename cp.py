#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 19:20:40 2019

@author: theobasili
"""
#%% import packages 
import numpy as np
from ase.cluster.wulff import wulff_construction
#from anim import make_3d_animation
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import progressbar
#%% Lattice construction
atoms = wulff_construction('Ar',                        # Type of Atom Ar r=2.63A
                           surfaces=[(1, 0, 0),         # fcc surfaces
                                     (1, 1, 1),
                                     (1, 1, 0)],
                           energies=[0.01, 0.1, 0.02],  # energies coupled to surfaces
                           size=63,                     # Approx number of atoms
                           structure='fcc',             # type of structure
                           rounding='closest')          # rounding number of atoms in order to fit into lattice
#%% Parameters
Nt =  1000               # Timesteps
nbins=100               # Number of bins for the histogram
dt = 0.01               # Stepsize
a=atoms.get_positions() # positions atoms
r=2.63                  # Size of atom (for normalisation)
a=a/r                   # normalized positions atoms
N=np.size(a,0)          # Number of atoms Lattice 
pos = np.zeros(shape=(Nt, N, 3), dtype=float)
pos[0] = a              # Initial Positions 
L=np.amax(pos)+(1/((0.45)**(1/3)/N**(1/3)))-np.amax(pos)        # Box size 
m = 1                   # mass of particle 6.6*10**(-26) kg
kb= 1                   # Boltzman constant 1.38064852*10**(-23) 
T = 1 #4.6                   # Temperature 119.8K  
sigma= 1                # Sigma 3.405*10**(-10)
epsilon= T*kb           # Epsilon
rho = N/L**3             # Rho
beta =1/(kb*T)          # Beta
#%% Initialisation 
np.random.seed(1)
vel=np.random.normal(loc=0,scale=1,size=(N, 3))
xpos=pos[0,:,0]
ypos=pos[0,:,1]
zpos=pos[0,:,2]
vx=vel.T[0]
vy=vel.T[1]
vz=vel.T[2]
GR=  np.zeros([Nt,nbins])
RR = np.zeros([Nt,N,N-1])    
dr=L/nbins
R = np.linspace(0, L, nbins+1)
R2 = R[:nbins]+dr/2
P = np.zeros(Nt)
K = np.zeros(Nt)
UU=np.zeros(Nt)
TE=np.zeros(Nt)
Cv = np.zeros(Nt)
n = 100 # bootstrap 
Pb = np.zeros(n)
Cvb = np.zeros(n)
timet = np.linspace(0,(Nt-1)*dt, Nt) # for plotting 
#%%  Definition of the Force function acting on particles
def F(xIn, yIn, zIn, nIn):
    Fx = np.zeros(nIn) # Force in the x direction 
    Fy = np.zeros(nIn) # Force in the y direction 
    Fz = np.zeros(nIn) # Force in the z direction 
    U  = np.zeros(nIn) # Potential Energy 
    r  = np.zeros([nIn,nIn-1]) # Distance
    
    xInList = xIn.tolist()
    yInList = yIn.tolist()
    zInList = zIn.tolist()
    # For every particle sum the forces from all other particles
    for i in np.arange(0,nIn):
        x = xInList[i]
        y = yInList[i]
        z = zInList[i]
        
        xnums1= xInList[0:i]
        xnums2=xInList[i+1:]
        xnums1.extend(xnums2)
        xvec = xnums1
    
        ynums1= yInList[0:i]
        ynums2=yInList[i+1:]
        ynums1.extend(ynums2)
        yvec = ynums1
    
        znums1= zInList[0:i]
        znums2= zInList[i+1:]
        znums1.extend(znums2)
        zvec = znums1
    
        xvec = np.asarray(xvec)
        yvec = np.asarray(yvec)
        zvec = np.asarray(zvec)
        
        xbold = (x - xvec + L/2) % L - L/2
        ybold = (y - yvec + L/2) % L - L/2
        zbold = (z - zvec + L/2) % L - L/2
        
        r[i,:] = np.sqrt(np.power(xbold,2) + np.power(ybold,2) + np.power(zbold,2))
       
        U[i] = 0.5 *4* np.sum(np.power(1/r[i,:],12) - np.power(1/r[i,:],6)) # potential E (the factor 0.5 prevents double counting)
       
        FFx = 4*((12/r[i,:]**14) - (6/r[i,:]**8))*xbold
        FFy = 4*((12/r[i,:]**14) - (6/r[i,:]**8))*ybold
        FFz = 4*((12/r[i,:]**14) - (6/r[i,:]**8))*zbold
        Fx[i] = np.sum(FFx)
        Fy[i] = np.sum(FFy)
        Fz[i] = np.sum(FFz)

    F_out = [Fx,Fy,Fz, U]
    FF = np.sqrt(FFx**2+FFy**2+FFz**2)
    rF_out = np.sum(r*FF)/2
    return F_out, r, rF_out;

[dvdt, r, rF] = F(xpos, ypos, zpos, N)
K[0]  = np.sum(0.5*epsilon*(vx**2+vy**2+vz**2))
UU[0] = dvdt[3][0]
TE[0] = K[0]+UU[0] 
Cv[0] = 1/( 2/(3*N)-(K[0]**2/K[0]**2-1) )
P[0]  = rho/beta* (1-beta/(3*N)*rF)
RR[0,:,:] = r
#%% Simulation computation 
with progressbar.ProgressBar(max_value=Nt) as bar:
    for t in np.arange(1,Nt):
        # Position with Verlet Algorithm
        xpos = xpos + vx * dt + (np.power(dt,2)*dvdt[0])/2
        ypos = ypos + vy * dt + (np.power(dt,2)*dvdt[1])/2
        zpos = zpos + vz * dt + (np.power(dt,2)*dvdt[2])/2
        # Construction of position matrix 
        pos[t,:,0]=xpos
        pos[t,:,1]=ypos
        pos[t,:,2]=zpos   
        # Velocity with Verlet Algorithm
        dvdt1, r, rF = F(xpos%L, ypos%L, zpos%L, N)
        vx =  vx + (dt/2)*(dvdt[0]+dvdt1[0]) 
        vy =  vy + (dt/2)*(dvdt[1]+dvdt1[1])
        vz =  vz + (dt/2)*(dvdt[2]+dvdt1[2])
        dvdt = dvdt1    
        # Energies
        K[t] = np.sum(0.5*epsilon*(vx**2+vy**2+vz**2)) # Kinetic energy
        UU[t] = np.sum(dvdt[3])                        # Potential energy
        TE[t]=K[t]+UU[t]                               # Total energy
        # Distances
        RR[t,:,:] = r
        # Lambda
        if t==1/dt-1: #or t==2*0.5/dt-1 or t==3*0.5/dt-1: #3 lambda iterations is enough 
        #    if t*dt%0.5==0: # (every 0.5)
                la=np.sqrt(((N-1)*3*kb*T)/(2*K[t]))
                vx=la*vx
                vy=la*vy
                vz=la*vz
        # Pressure
        P[t] = rho/beta* (1-beta/(3*N)*-rF)
        # Pair correlation function
        NR = np.asarray(list(np.histogram(RR[t-1],R)))[0]
        NR = NR/Nt
        GR[t-1,:] = NR*2*L**3/(N*(N-1)*4*np.pi*dr*R2**2) 
        bar.update(t)
# Average observables
P_mean = np.mean(P)       # Pressure
Cv_mean = 1/( 2/(3*N)-(np.mean(K**2)/np.mean(K)**2-1) )  # Specific heat
MSD = np.mean((pos[:,:,0]-pos[0,:,0])**2, 1)              # Diffusion
# Bootstrap
for i in range(n):
    Kb = np.random.choice(K, size=Nt, replace=True)
    PPb = np.random.choice(P, size=Nt, replace=True)
    Pb[i] = np.mean(PPb)
    Cvb[i] = 1/( 2/(3*N)-(np.mean(Kb**2)/np.mean(Kb)**2-1) )
sigma_Pb = np.sqrt(np.mean(Pb**2)-np.mean(Pb)**2)
sigma_Cvb = np.sqrt(np.mean(Cvb**2)-np.mean(Cvb)**2)
# Correlation time
xa=np.zeros([Nt,nbins])
for t in np.arange(0,Nt-1):
    ff=1/(Nt-1-t)
    s1=np.sum(GR[:Nt-t,:]*GR[t:,:],0)
    s2=np.sum(GR[:Nt-t,:],0)
    s3=np.sum(GR[t:,:],0)
    xa[t,:]=ff*s1-ff*s2*ff*s3
def func(t,a, tau):
    return a*np.exp(-t/tau) 
#Fit
n0=20
t=np.arange(0,n0)
rcol=np.where(xa==xa.max())[1][0]
xguess=max(xa[:,rcol])
guess=[xguess,int(0.05/dt)]
y=xa[:n0,rcol]
par = curve_fit(func, t, y,guess)[0]
yy=func(t, *par)
tau = par[1]
# Data blocking
Block = int(np.ceil(2*tau))
N_blocks = int(np.floor(Nt/Block))
P_db = np.zeros(N_blocks)
Cv_db = np.zeros(N_blocks)
for i in range(N_blocks):
    tt = np.arange(Block)+i*Block
    P_db[i] = np.mean(P[tt])   # mean Pressure
    Cv_db[i] = 1/( 2/(3*N)-(np.mean(K[tt]**2)/np.mean(K[tt])**2-1) )   # Specific heat
P_mean_db = np.mean(P_db)
Cv_mean_db = np.mean(Cv_db)
sigma_P_db = np.sqrt(np.mean(P_db**2)-np.mean(P_db)**2)
sigma_Cv_db = np.sqrt(np.mean(Cv_db**2)-np.mean(Cv_db)**2)
#%% Plotting
# Animation
plt.close('all')
ee = np.asarray(list(np.histogram(RR,nbins)))[0] # Histogram
ee=ee/Nt/2
plt.figure(1)
plt.bar(R[:-1],ee)
plt.legend(["Number of bins ="+str(nbins)])
plt.title("Histogram")
GRR=np.zeros(nbins-1) # Pair correlation function
for nb in range(nbins-1):
    GRR[nb]=np.mean(GR.T[nb,:])
plt.figure(2)
plt.plot(GRR)
plt.title("Pair Correlation Function")
plt.figure(3)
plt.plot(timet, K/N)   # Ekin
plt.plot(timet, UU/N)  # Epot
plt.plot(timet, TE/N)  # Etot
plt.title("Energy Plot")
plt.xlabel('time')
plt.ylabel('energy')
plt.legend(["Kinetic Energy","Potential Energy","Total Energy"])
plt.figure(4)
plt.plot(timet, P)   # Pressure
plt.title("Pressure")
plt.figure(5)
plt.plot(timet, MSD) # Diffusion
plt.title("Diffusion")
plt.ylabel('MSD')
plt.figure(6)
plt.plot(t,yy,'r--') # Error Estimation
plt.plot(t,y,'k')
plt.axis([0, n0, 0, np.amax(y)])
plt.title("Error Estimation Fit")
plt.legend(["Exponential fit curve","Autocorrelation function"])
#anim = make_3d_animation(L, pos%L, delay=50, rotate_on_play=0)
#plt.title("Simulation")